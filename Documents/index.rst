.. NEMO_EASTCOAST documentation master file, created by
   sphinx-quickstart on Thu Aug 14 11:25:42 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

********************************************
GETM_DOCS
********************************************
This is a collection of my oceanography documents and files, mainly on GETM 

Contents
=========

.. toctree::
   :maxdepth: 1

   GETM/index
 

